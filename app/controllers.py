from app import app
from flask import request
from heplers import verify_token, write_to_hosts, write_to_ansible_host
import json
import os

@app.route("/install", methods=['POST'])
def get_jupyter_token():
    token = request.headers['Authorization']
    response_data = {}
    if verify_token(token) is None:
        response_data['verify_token'] = False
    else:
        response_data['verify_token'] = True
        ansible_ip = request.form['ansible_ip']
        master_ip = request.form['master_ip']
        worker_ips = request.form.getlist('worker_ip')

        if len(worker_ips) < 1:
            response_data['success'] = False
            response_data['message'] = 'Invalid number of workers'
        else:
            write_to_hosts(ansible_ip, master_ip, worker_ips)
            write_to_ansible_host(ansible_ip, master_ip, worker_ips)

            try:
                os.system("sudo ansible-playbook -s /home/ubuntu/QTLaaS/spark_deployment.yml")
                response_data['success'] = True
            except Exception as e:
                response_data['success'] = False
                response_data['message'] = e.message

    return json.dumps(response_data)
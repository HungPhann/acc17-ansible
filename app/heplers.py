# app/helpers.py

import jwt
import app
from run import app

def verify_token(token):
    try:
        payload = jwt.decode(token, app.config['SECRET_KEY'], algorithms=['HS256'])
    except:
        return None
    return payload['user_id']
    # Signature has expired

def write_to_hosts(ansible_ip, master_ip, worker_ips):
    original_file = '''127.0.0.1 localhost

# The following lines are desirable for IPv6 capable hosts
::1 ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
ff02::3 ip6-allhosts'''

    data = "" + str(ansible_ip) + "\n" + str(master_ip) + "\n"
    for worker_ip in worker_ips:
        data += worker_ip + "\n"
    
    data += original_file
    
    with open("/etc/hosts", "w") as f:
        f.write(data)

def write_to_ansible_host(ansible_ip, master_ip, worker_ips):
    data = "ansible-node ansible_ssh_host=" + str(ansible_ip) + "\n"
    data += "sparkmaster ansible_ssh_host=" + str(master_ip) + "\n"

    for i in range (1, len(worker_ips)+1):
        data += "sparkworker" + str(i) + " ansible_ssh_host=" + str(worker_ips[i-1]) + "\n"

    data += '''
[configNode]
ansible-node ansible_connection=local ansible_user=ubuntu

[sparkmaster]
sparkmaster ansible_connection=ssh ansible_user=ubuntu

[sparkworker]
'''

    data += "sparkworker[1:" + str(len(worker_ips)) + "]" + " ansible_connection=ssh ansible_user=ubuntu"

    with open("/etc/ansible/hosts", "w") as f:
        f.write(data)


    